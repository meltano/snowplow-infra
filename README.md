# snowplow-infra

## Instructions

### Initialize Terraform dependencies

```bash
terraform init
```

### Validate and format code

```bash
terraform fmt
terraform validate
```

### Plan

```bash
terraform plan
```

Note: this is currently using _local_ state but eventually will use an S3 backend for production.
