/*
The Collector module receives events and pushes them to a Kinesis stream
for storage and further processing.

Events are sent by Trackers, which are just client- or server-side libraries
that enable us to collect events from our applications.

https://docs.snowplowanalytics.com/docs/collecting-data/collecting-from-own-applications/
*/

module "collector_lb" {
  source  = "snowplow-devops/alb/aws"
  version = "0.1.2"

  name              = "${var.prefix}-collector-lb"
  vpc_id            = var.vpc_id
  subnet_ids        = var.public_subnet_ids
  health_check_path = "/health"

  ssl_certificate_arn     = var.ssl_certificate_arn
  ssl_certificate_enabled = var.ssl_certificate_enabled

  tags = var.tags
}

module "collector_kinesis" {
  source  = "snowplow-devops/collector-kinesis-ec2/aws"
  version = "0.2.0"

  name               = "${var.prefix}-collector-server"
  vpc_id             = var.vpc_id
  subnet_ids         = var.public_subnet_ids
  collector_lb_sg_id = module.collector_lb.sg_id
  collector_lb_tg_id = module.collector_lb.tg_id
  ingress_port       = module.collector_lb.tg_egress_port
  good_stream_name   = module.raw_stream.name
  bad_stream_name    = module.bad_1_stream.name

  ssh_key_name = aws_key_pair.pipeline.key_name
  ssh_ip_allowlist = [
    "0.0.0.0/0",
  ]

  telemetry_enabled = var.snowplow_telemetry_enabled
  user_provided_id  = var.snowplow_telemetry_user_id

  iam_permissions_boundary = var.iam_permissions_boundary

  tags = var.tags

  cloudwatch_logs_enabled        = var.cloudwatch_logs_enabled
  cloudwatch_logs_retention_days = var.cloudwatch_logs_retention_days
}
