/*
Raw, enriched and bad events are stored in S3, processed by the Snowplow S3 loaders.

https://docs.snowplowanalytics.com/docs/pipeline-components-and-applications/loaders-storage-targets/s3-loader/
*/

resource "aws_s3_bucket" "snowplow_bucket" {
  bucket = "${var.prefix}-snowplow-bucket"
  acl    = "private"

  tags = var.tags
}

module "s3_loader_raw" {
  source  = "snowplow-devops/s3-loader-kinesis-ec2/aws"
  version = "0.2.0"

  name             = "${var.prefix}-s3-loader-raw-server"
  vpc_id           = var.vpc_id
  subnet_ids       = var.public_subnet_ids
  in_stream_name   = module.raw_stream.name
  bad_stream_name  = module.bad_1_stream.name
  s3_bucket_name   = aws_s3_bucket.snowplow_bucket.id
  s3_object_prefix = "${var.s3_bucket_object_prefix}raw/"

  ssh_key_name = aws_key_pair.pipeline.key_name
  ssh_ip_allowlist = [
    "0.0.0.0/0",
  ]

  telemetry_enabled = var.snowplow_telemetry_enabled
  user_provided_id  = var.snowplow_telemetry_user_id

  iam_permissions_boundary = var.iam_permissions_boundary

  kcl_write_max_capacity = var.pipeline_kcl_write_max_capacity

  tags = var.tags

  cloudwatch_logs_enabled        = var.cloudwatch_logs_enabled
  cloudwatch_logs_retention_days = var.cloudwatch_logs_retention_days
}

module "s3_loader_bad" {
  source  = "snowplow-devops/s3-loader-kinesis-ec2/aws"
  version = "0.2.0"

  name             = "${var.prefix}-s3-loader-bad-server"
  vpc_id           = var.vpc_id
  subnet_ids       = var.public_subnet_ids
  in_stream_name   = module.bad_1_stream.name
  bad_stream_name  = module.bad_2_stream.name
  s3_bucket_name   = aws_s3_bucket.snowplow_bucket.id
  s3_object_prefix = "${var.s3_bucket_object_prefix}bad/"

  ssh_key_name = aws_key_pair.pipeline.key_name
  ssh_ip_allowlist = [
    "0.0.0.0/0",
  ]

  telemetry_enabled = var.snowplow_telemetry_enabled
  user_provided_id  = var.snowplow_telemetry_user_id

  iam_permissions_boundary = var.iam_permissions_boundary

  kcl_write_max_capacity = var.pipeline_kcl_write_max_capacity

  tags = var.tags

  cloudwatch_logs_enabled        = var.cloudwatch_logs_enabled
  cloudwatch_logs_retention_days = var.cloudwatch_logs_retention_days
}

module "s3_loader_enriched" {
  source  = "snowplow-devops/s3-loader-kinesis-ec2/aws"
  version = "0.2.0"

  name             = "${var.prefix}-s3-loader-enriched-server"
  vpc_id           = var.vpc_id
  subnet_ids       = var.public_subnet_ids
  in_stream_name   = module.enriched_stream.name
  bad_stream_name  = module.bad_1_stream.name
  s3_bucket_name   = aws_s3_bucket.snowplow_bucket.id
  s3_object_prefix = "${var.s3_bucket_object_prefix}enriched/"

  ssh_key_name = aws_key_pair.pipeline.key_name
  ssh_ip_allowlist = [
    "0.0.0.0/0",
  ]

  telemetry_enabled = var.snowplow_telemetry_enabled
  user_provided_id  = var.snowplow_telemetry_user_id

  iam_permissions_boundary = var.iam_permissions_boundary

  kcl_write_max_capacity = var.pipeline_kcl_write_max_capacity

  tags = var.tags

  cloudwatch_logs_enabled        = var.cloudwatch_logs_enabled
  cloudwatch_logs_retention_days = var.cloudwatch_logs_retention_days
}
