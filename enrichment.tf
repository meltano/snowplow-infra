/*
The Enrichment module handles aspects like

- IP Anonymization
- IP lookups
- PII obfuscation

https://docs.snowplowanalytics.com/docs/enriching-your-data/what-is-enrichment/
https://docs.snowplowanalytics.com/docs/enriching-your-data/available-enrichments/
*/

module "enrich_kinesis" {
  source  = "snowplow-devops/enrich-kinesis-ec2/aws"
  version = "0.2.0"

  name                 = "${var.prefix}-enrich-server"
  vpc_id               = var.vpc_id
  subnet_ids           = var.public_subnet_ids
  in_stream_name       = module.raw_stream.name
  enriched_stream_name = module.enriched_stream.name
  bad_stream_name      = module.bad_1_stream.name

  enrichment_ua_parser_config      = jsonencode(file("${path.module}/enrichments/ua_parser.json"))
  enrichment_ip_lookups            = jsonencode(file("${path.module}/enrichments/ip_lookups.json"))
  enrichment_anon_ip               = jsonencode(file("${path.module}/enrichments/anon_ip.json"))
  enrichment_referer_parser        = jsonencode(file("${path.module}/enrichments/referer_parser.json"))
  enrichment_pii_enrichment_config = jsonencode(file("${path.module}/enrichments/pii.json"))

  ssh_key_name = aws_key_pair.pipeline.key_name
  ssh_ip_allowlist = [
    "0.0.0.0/0",
  ]

  iam_permissions_boundary = var.iam_permissions_boundary

  telemetry_enabled = var.snowplow_telemetry_enabled
  user_provided_id  = var.snowplow_telemetry_user_id

  # Linking in the custom Iglu Server here
  custom_iglu_resolvers = []

  kcl_write_max_capacity = var.pipeline_kcl_write_max_capacity

  tags = var.tags

  cloudwatch_logs_enabled        = var.cloudwatch_logs_enabled
  cloudwatch_logs_retention_days = var.cloudwatch_logs_retention_days
}
