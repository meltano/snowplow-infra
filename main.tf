// Configure state
terraform {
  required_version = "~> 1.0.0"
  # TODO: swith to S3-backed state
  backend "s3" {
    bucket         = "tf-remote-state20211117123028975000000006"
    dynamodb_table = "tf-remote-state-lock"
    encrypt        = true
    key            = "snowplow/terraform.tfstate"
    region         = "us-east-1"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

// Use credentials from environment or shared credentials file
provider "aws" {
  region = "us-east-1"
}

resource "aws_key_pair" "pipeline" {
  key_name   = "${var.prefix}-pipeline"
  public_key = var.ssh_public_key
}
